import React from 'react';
import configureStore from './configureStore';
import MainPage from './pages/MainPage';
import {Provider} from 'react-redux'
const store = configureStore();
function Root() {
  return (
    <Provider store={store}>
      <MainPage />
    </Provider>
  );
}

export default Root;
