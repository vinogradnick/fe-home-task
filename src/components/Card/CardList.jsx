import React, { Component } from 'react'
import Card from './Card'
export default class CardList extends React.Component {
    constructor(props){
        super(props);
        console.log(this.props);
    }
    render() {
        const {cards}=this.props;
        return (
            <div className="card-list-container">
        {cards && cards.map((card,index)=><Card card={card}  key={index} />)}
            </div>
        )
    }
}
