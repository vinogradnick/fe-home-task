import React, { useState, useEffect } from 'react'
function Card(props) {
    const { image, text, category } = props.card;
    const [inversionClass, setInversionClass] = useState('card-light');
    const invertate = () => {
        setInversionClass(inversionClass == 'card-dark' ? 'card-light' : 'card-dark');
    };

    const [hoverShadow, setHover] = useState('');
    const hoverEnable = () => setHover('card-shadow');
    const hoverDisable = () => setHover('');

    return (
        <div 
        className={'card ' + inversionClass + ' ' + hoverShadow} 
        onClick={() => invertate()} 
        onMouseOver={() => hoverEnable()} 
        onMouseOut={() => hoverDisable()}>
            <img src={image} alt="some image description" className="card-image" />
            <div className="card-content">
                <div className="card-category">
                    <p className="category-text">
                        {category}
                    </p>
                </div>
                <p className="card-content-text">
                    {text}
                </p>
            </div>
        </div>
    )
}

export default Card;
