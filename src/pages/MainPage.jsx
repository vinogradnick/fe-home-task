import React from 'react'
import CardContainer from '../containers/CardContainer';

export default function MainPage() {
    return (
        <div className="main-page">
            <CardContainer/>
        </div>
    )
}
