import { SORT_CARD_LIST, FETCH_CARD_LIST } from '../constants/card';
import { axiosConfig } from '../api/axiosConfig';


export const fetchCardList = (cards) => {
    console.log('ff');
    return {
        type: FETCH_CARD_LIST,
        payload:cards
    }
}


export const fetchCards = () => async (dispatch) =>
    axiosConfig.get('cards')
        .then(resp => dispatch(fetchCardList(resp.data)));
