import { FETCH_CARD_LIST } from "../constants/card";

export default (state={},action)=>{
    switch(action.type){
        case FETCH_CARD_LIST:
            return ({
                ...state,
                cards:action.payload
            })
            default: return state;
    }
}
