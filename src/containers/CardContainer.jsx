import React, { Component } from 'react'
import {connect} from 'react-redux';
import {fetchCards} from '../actions/card'
import {getCards} from '../reducers/card';
import CardList from '../components/Card/CardList';

class CardContainer extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(){
        const {fetchCards} =this.props;
        fetchCards();
    }

  render() {
      const {cards}=this.props;
        return (
           <CardList cards={cards} />
        )
    }
}


const mapStateToProps=state=>({cards:state.cardReducer.cards});
const mapDispatchToProps={
    fetchCards
};

export default connect(mapStateToProps,mapDispatchToProps)(CardContainer);