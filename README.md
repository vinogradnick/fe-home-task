### Установка проекта 

```bash
 npm install
```
### Для загрузки списка карточек используется json-server
```bash
 json-server --watch cards.json
```
### Запуск проекта 
```
npm start
